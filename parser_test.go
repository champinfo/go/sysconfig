package sysconfig

import (
	"github.com/ompluscator/dynamic-struct"
	"testing"
)

func TestBuildStruct(t *testing.T) {
	instance, err := BuildStruct("config.json")
	if err != nil {
		t.Errorf(`build struct failed %#v`, err)
	}

	reader := dynamicstruct.NewReader(instance)

	if value := reader.GetField("CacheRefreshEvery").Int(); value != 10 {
		t.Errorf(`TestExample - expected field "CacheRefreshEvery" to be %#v got %#v`, 10, value)
	}

	if value := reader.GetField("UploadFileMaxSize").Int(); value != 100 {
		t.Errorf(`TestExample - expected field "UploadFileMaxSize" to be %#v got %#v`, 100, value)
	}

	dbReader := dynamicstruct.NewReader(reader.GetField("DataBase").Interface())
	if value := dbReader.GetField("Password").String(); value != "secret" {
		t.Errorf(`TestExample - expected field "Password" to be %#v got %#v`, "secret", value)
	}
	if value := dbReader.GetField("DBName").String(); value != "members" {
		t.Errorf(`TestExample - expected field "DBName" to be %#v got %#v`, "members", value)
	}
	if value := dbReader.GetField("IsDebug").Bool(); value != true {
		t.Errorf(`TestExample - expected field "IsDebug" to be %#v got %#v`, true, value)
	}
	if value := dbReader.GetField("MaxIdleConnections").Int(); value != 1024 {
		t.Errorf(`TestExample - expected field "MaxIdleConnections" to be %#v got %#v`, 1024, value)
	}

	networkReader := dynamicstruct.NewReader(reader.GetField("Network").Interface())
	if value := networkReader.GetField("Host").String(); value != "0.0.0.0" {
		t.Errorf(`TestExample - expected field "Host" to be %#v got %#v`, "0.0.0.0", value)
	}
	if value := networkReader.GetField("Port").String(); value != "8090" {
		t.Errorf(`TestExample - expected field "Port" to be %#v got %#v`, "8090", value)
	}
	if value := networkReader.GetField("Protocol").String(); value != "tcp4" {
		t.Errorf(`TestExample - expected field "Protocol" to be %#v got %#v`, "tcp4", value)
	}
}
