module sysconfig

go 1.13

require (
	github.com/emirpasic/gods v1.12.0
	github.com/ompluscator/dynamic-struct v1.2.0
)
