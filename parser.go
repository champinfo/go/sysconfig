package sysconfig

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/ompluscator/dynamic-struct"
	"io/ioutil"
	"os"
	"path"
	"runtime"

	"github.com/emirpasic/gods/stacks/arraystack"
)

var builderStack *arraystack.Stack
var keyStack *arraystack.Stack

//readConfigFile 讀取config.json檔案並進行內容分析
func readConfigFile(file string) ([]byte, map[string]interface{}, error) {
	_, filePath, _, _ := runtime.Caller(0)

	configPath := path.Join(path.Dir(filePath), file)

	info, err := os.Stat(configPath)
	if os.IsNotExist(err) {
		return nil, nil, fmt.Errorf("%#v does not exist", file)
	}
	if info.IsDir() {
		return nil, nil, fmt.Errorf("%#v is directory not a file", file)
	}

	// read the file
	data, err := ioutil.ReadFile(configPath)
	if err != nil {
		return nil, nil, fmt.Errorf("read %#v failed %#v", file, err)
	}

	if json.Valid(data) == false {
		return nil, nil, fmt.Errorf("%#v is not valid json format", file)
	}

	var jsonConfigs map[string]interface{}

	if err := json.Unmarshal(data, &jsonConfigs); err != nil {
		return nil, nil, fmt.Errorf("unmarshal %#v failed %#v", file, err)
	}

	return data, jsonConfigs, nil
}

func BuildStruct(configFile string) (interface{}, error) {
	data, config, err := readConfigFile(configFile)

	if err != nil {
		return nil, err
	}

	if len(data) == 0 {
		return nil, errors.New("the data can not be empty")
	}

	// instantiate two stacks
	builderStack = arraystack.New()
	keyStack = arraystack.New()

	rootBuilder := dynamicstruct.NewStruct()
	iterateTypesMap(config, &rootBuilder)

	// iterate the stack
	for builderStack.Size() > 0 {
		subBuilderPtr, _ := builderStack.Pop()
		key, _ := keyStack.Pop()

		subInstance := (*subBuilderPtr.(*dynamicstruct.Builder)).Build().New()
		rootBuilder.AddField(key.(string), subInstance, "")
	}
	// generate the instance
	configInstance := rootBuilder.Build().New()

	err = json.Unmarshal(data, &configInstance)
	if err != nil {
		return nil, err
	}
	return configInstance, nil
}

func iterateTypesMap(types map[string]interface{}, builder *dynamicstruct.Builder) {
	for k, v := range types {
		switch t := v.(type) {
		// To unmarshal JSON into an interface value, Unmarshal stores one of these in the interface value:
		// float64, for JSON numbers
		// however this method also convert the real float to int!!
		case float64:
			nv := int(v.(float64))
			(*builder).AddField(k, nv, fmt.Sprintf("`json:\"%s\"`", k))
		case int, int8, int16, int32, int64, string, bool:
			(*builder).AddField(k, v, fmt.Sprintf("`json:\"%s\"`", k))
		case map[string]interface{}:
			subBuilder := dynamicstruct.NewStruct()
			keyStack.Push(k)
			builderStack.Push(&subBuilder)
			iterateTypesMap(t, &subBuilder)
		}
	}
}
